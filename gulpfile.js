// project paths are set in package.json
const paths = require("./package.json").paths;

const gulp = require("gulp");
const postcss = require("gulp-postcss");
const purgecss = require("gulp-purgecss");
const tailwindcss = require("tailwindcss");
const browserSync = require("browser-sync").create();
var rimraf = require('rimraf').sync;
var panini = require('panini');

// compiling tailwind CSS
gulp.task("css", () => {
  return gulp
    .src(paths.src.css + "*.css")
    .pipe(
      postcss([tailwindcss(paths.config.tailwind), require("autoprefixer")])
    )
    .pipe(gulp.dest(paths.dist.css))
    .on('finish', browserSync.reload);
});

// compile panini files
gulp.task('html', function() {
  gulp.src(paths.src.html + 'pages/**/*.html')
    .pipe(panini({
      root: paths.src.html + 'pages/',
      layouts: paths.src.html + 'layouts/',
      partials: paths.src.html + 'includes/',
      helpers: paths.src.html + 'helpers/',
      data: paths.src.html + 'data/'
    }))
    .pipe(gulp.dest('dist'))
    .on('finish', browserSync.reload);
});

// Remove everything
gulp.task('clean', function() {
  rimraf(paths.dist.base);
});


gulp.task('html:reset', function(done) {
  panini.refresh();
  done();
});


// browser-sync dev server
gulp.task("serve", ["css", "html"], () => {
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  });

  gulp.watch(paths.src.css + "*.css", ["css"]);
  gulp.watch(paths.config.tailwind, ["css"]);
  gulp.watch(paths.src.html + 'pages/**/*', ["html"]);
  gulp.watch([paths.src.html + '{layouts,includes,helpers,data}/**/*'], ["html:reset","html"]);
});

// default task
gulp.task("default", ["serve"]);
